﻿using System;
using System.Activities;

namespace NumberGuessWorkflow.Activities
{
    public sealed class ReadInt : NativeActivity<int>
    {
        [RequiredArgument]
        public InArgument<string> BookmarkName { get; set; }

        protected override void Execute(NativeActivityContext context)
        {
            string name = BookmarkName.Get(context);
            if (string.IsNullOrEmpty(name))
            {
                throw new ArgumentException("BookmarkName cannot be null or empty.", "BookmarkName");
            }
            context.CreateBookmark(name, new BookmarkCallback(OnReadComplete));
            
        }

        protected override bool CanInduceIdle => true;

        private void OnReadComplete(NativeActivityContext context, Bookmark bookmark, object state)
        {
            this.Result.Set(context, Convert.ToInt32(state));
        }
    }
}