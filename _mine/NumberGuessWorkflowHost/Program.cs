﻿using System.IO;
using System.Reflection;
using System.Windows.Forms;
using Frontier.Extensions.Log.Config;
using log4net;
using log4net.Config;
using log4net.Core;
using NumberGuessWorkflow.BL;
using NumberGuessWorkflow.Forms;

namespace NumberGuessWorkflow
{
    internal class Program
    {
        private static ILog Log;

        static void Main(string[] args)
        {
            XmlConfigurator.Configure(new FileInfo(Log4NetConfig.Settings.Path));
            Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

            //StartConsonle(args);
            StartWinForm(args);
        }

        private static void StartConsonle(string[] args = null)
        {
            Log.Info("Test");
            Level.Info.Log("Test");

            var wf = new WF();
            wf.SetupComponents();
            wf.Run();
        }

        private static void StartWinForm(string[] args = null)
        {
            Log.Info("Test");
            Application.EnableVisualStyles();
            Application.Run(new WorkflowHostForm());
        }
    }
}