# README #

My Travels through learning Workflow Foundation 4.5

### What is this repository for? ###

* [Windows Workflow Foundation (WF45) Getting Started Tutorial](https://code.msdn.microsoft.com/Windows-Workflow-deed2cd5)
* [Windows Communication Foundation (WCF) and Windows Workflow Foundation (WF) Samples for .NET Framework 4](https://www.microsoft.com/en-us/download/confirmation.aspx?id=21459)

### PROJECT DESCRIPTION ###

This project is a completed version of the 7 steps of the [WF45 Getting Started Tutorial](http://msdn.microsoft.com/en-us/library/dd489454(VS.110).aspx).

(For a WF4 version of the tutorial, including a video walkthrough for each of the steps in the tutorial, see [WF4 Getting Started Tutorial](http://aka.ms/wf4gettingstarted).)

This tutorial illustrates:

*   How to create custom activities using both composition and code
*   How to create State Machine, Flowchart, and Sequential workflows
*   How to host workflows using [WorkflowApplication](http://msdn.microsoft.com/en-us/library/system.activities.workflowapplication.aspx)
*   How to create a persistence database and host long running workflows
*   How to create a custom TrackingParticipant
*   How to use WorkflowIdentity to host multiple versions of a workflow side-by-side
*   How to use Dynamic Update to modify the definition of a running workflow instance

Step 1, [How to: Create an Activity](http://msdn.microsoft.com/en-us/library/dd489453(VS.110).aspx), demonstrates how to create a simple activity by using code to implement its execution logic and an activity whose implementation is defined by using other activities.

Step 2, [How to: Create a Workflow](http://msdn.microsoft.com/en-us/library/dd489437(VS.110).aspx), demonstrates how to create a workflow using either a Flowchart workflow, a Sequential workflow, or a State Machine workflow, using built-in activities and the custom activities from the first step in the tutorial.

Step 3, [How to: Run a Workflow](http://msdn.microsoft.com/en-us/library/dd489463(VS.110).aspx), demonstrates how to host a workflow using [WorkflowApplication](http://msdn.microsoft.com/en-us/library/system.activities.workflowapplication.aspx). It shows how to pass data into a workflow when starting the workflow, how to retrieve data from a completed workflow, and how to pass data into a running workflow using bookmarks.

Step 4, [How to: Create and Run a Long Running Workflow](http://msdn.microsoft.com/en-us/library/dd489452.aspx), demonstrates how to configure a workflow persistence database, and how to host a long running workflow in a Windows Forms host.

Step 5, [How to: Create a Custom Tracking Participant](http://msdn.microsoft.com/en-us/library/jj205426.aspx), demonstrates how to create a custom tracking participant that is used to display status about the progress of the workflow in the host application.

Step 6, [How to: Host Multiple Versions of a Workflow Side-by-Side](http://msdn.microsoft.com/en-us/library/jj205425.aspx), demonstrates how to use the new versioning features in WF45 to host multiple versions of a workflow side-by-side in the same host application.

Step 7, [How to: Update the Definition of a Running Workflow Instance](http://msdn.microsoft.com/en-us/library/jj205427.aspx), demonstrates how to use Dynamic Update to update the definition of a running workflow instance.